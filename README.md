# Litecoin Core

A Litecoin docker image.

## Tags

- `0.18.1` ([0.18.1/GitHub](https://github.com/litecoin-project/litecoin/releases/tag/v0.18.1))
- `0.17.1` ([0.17.1/GitHub](https://github.com/litecoin-project/litecoin/releases/tag/v0.17.1))

## What is Litecoin Core?

Litecoin Core is the Litecoin reference client and contains all the protocol rules required for the Litecoin network to function. This client is used by mining pools, merchants and services all over the world for its rock solid stability, feature set and security. Learn more about [Litecoin Core](https://litecoincore.org/).

## Usage

### How to use this image

You must run a container with predefined environment variables.

Create `.env` file with following variables:

| Env variable                    | Description                                                                |
| ------------------------------- | -------------------------------------------------------------------------- |
| `RPC_USER_NAME`                 | User to secure the JSON-RPC api.                                           |
| `RPC_USER_PASSWORD`             | A password to secure the JSON-RPC api.                                     | 
| `PATH_TO_DATA_FOLDER`           | Full path to the blockchain data folder.                                   |

Run the container: 

```bash
docker-compose up -d 

```

Stop the container: 

```bash
docker-compose down 

```
